package drivers;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.ResourceBundle;

import org.testng.annotations.Test;

import utilities.DataHandler;

public class DriverScript {

	Object[] arguments;
	ResourceBundle functionmappings = ResourceBundle.getBundle("utilities.FunctionMapping");
	// Object noObjs[] = {};
	Class noparams[] = {};

	@Test
	public void scenarioExecutor() throws Exception {
		arguments = null;
		DataHandler dh = new DataHandler();

		ArrayList<LinkedHashMap<String, String>> scenarioData = dh.readDataFromSheet();
		String[] paramList = null;
		String functionName = null;
		// Get Class using reflection
		Class<?> stepClass = DriverScript.class;
		for (int i = 0; i < scenarioData.size(); i++) {
			LinkedHashMap<String, String> scenarioRow = scenarioData.get(i);

			for (String stepName : scenarioRow.values()) {

				if (stepName.trim().contains("Chrome") || stepName.trim().contains("IE") || stepName.trim().contains("FireFox")) {


					System.setProperty("testBrowser",stepName);
				}

				else {

					if (stepName.trim().contains("(")) {

						String[] splitFunction = stepName.split("\\(");
						functionName = splitFunction[0];
						String params = splitFunction[1].replace(")", "");
						// String classPath = functionmappings .getString(functionName);
						// stepClass = Class.forName(classPath);
						// Object obj = stepClass.newInstance();

						if (params.contains(",")) {
							paramList = params.split(",");
							arguments = new Object[paramList.length];
							for (int j = 0; j < paramList.length; j++) {
								if (paramList[j].contains("\"")) {
									arguments[j] = paramList[j].replace("\"", "");
								} else {
									arguments[j] = Integer.parseInt(paramList[j]);
								}
							}

						} else {
							arguments = new Object[1];
							if (params.contains("\"")) {
								arguments[0] = params.replace("\"", "");
							} else {
								arguments[0] = Integer.parseInt(params);
							}
						}
					} else {
						functionName = stepName;
					}

					String functionClass = functionmappings.getString(functionName.trim());
					Class<?> c = Class.forName(functionClass);

					Constructor cons = null;
					Object obj = null;

					cons = c.getConstructor();

					obj = cons.newInstance();

					// Get all methods
					Method[] allMethods = c.getMethods();

					// Traverse all methods
					for (Method m : allMethods) {
						String mname = m.getName();
						if (mname.equals(functionName)) {
							// Type[] pType = m.getParameterTypes();
							m.invoke(obj, arguments);
							break;
						} // End of -- if (mname.equals(functionNamespace))
					} // End of -- for (Method m : allMethods)

					arguments = null;
				}
			}
		}
	}
}
