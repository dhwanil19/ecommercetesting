package utilities;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;

public class DataHandler {
	DataUtility du;

	public DataHandler() {
		du = new DataUtility();
	}

	public ArrayList<LinkedHashMap<String, String>> readDataFromSheet() {
		ArrayList<LinkedHashMap<String, String>> sheetData = new ArrayList<LinkedHashMap<String, String>>();
		LinkedHashMap<String, String> singleRowData = new LinkedHashMap<String, String>();
		int lastRowCount = du.getLastRowNum("ScenarioSheet");

		for (int i = 1; i <= lastRowCount; i++) {
			singleRowData = du.getSingleRowData("ScenarioSheet", i);
			if (singleRowData != null) {
				sheetData.add(singleRowData);
			}
		}
		return sheetData;

	}
}
