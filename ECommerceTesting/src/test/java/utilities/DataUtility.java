package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.LinkedHashMap;
import java.util.ResourceBundle;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataUtility {

	// To be made abstract
	public String filePath;
	public ResourceBundle prop;
	public XSSFWorkbook wb;

	public DataUtility() {
		prop = ResourceBundle.getBundle("dataPath");
		String filePath = prop.getString("dataSheetPath");

		try {
			File file = new File(filePath);
			FileInputStream fs = new FileInputStream(file);
			wb = new XSSFWorkbook(fs);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public int getHeaderCount(XSSFSheet sh) {
		XSSFRow row = sh.getRow(0);
		return row.getLastCellNum();
	}

	public int getLastRowNum(String sheetName) {
		XSSFSheet sh = wb.getSheet(sheetName);
		return sh.getLastRowNum();
	}

	public XSSFSheet getSheet(String sheetName) {
		XSSFSheet sh = wb.getSheet(sheetName);
		return sh;
	}

	public LinkedHashMap<String, String> getSingleRowData(String sheetName, int rowNum) {
		LinkedHashMap<String, String> dataRow = new LinkedHashMap<String, String>();

		XSSFSheet sh = wb.getSheet(sheetName);

		int headerCount = getHeaderCount(sh);

		XSSFRow headerRow = sh.getRow(0);

		String executionIndicator = sh.getRow(rowNum).getCell(1).toString();

		if (executionIndicator.equalsIgnoreCase("Yes")) {
			//Read Scenario name
			String scenarioName = sh.getRow(rowNum).getCell(2).toString();

			//			//Read browser name
			//			String browser = sh.getRow(rowNum).getCell(3).toString();
			//

			for (int j = 3; j < headerCount; j++) {
				XSSFCell cell = sh.getRow(rowNum).getCell(j);
				dataRow.put(scenarioName + "_" + headerRow.getCell(j).toString(), cell.toString());
			}
			return dataRow;
		} else {
			return null;
		}

	}


}
