package utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class InitializeTestBrowser {

	// instance of singleton class
	private static InitializeTestBrowser instanceOfTestBrowserClass = null;

	// TO create instance of class
	public static InitializeTestBrowser getinstanceOfTestBrowserClass(String browser) {
		if (instanceOfTestBrowserClass == null) {
			instanceOfTestBrowserClass = new InitializeTestBrowser(browser);
		}
		return instanceOfTestBrowserClass;
	}

	private WebDriver driver;

	// Constructor
	private InitializeTestBrowser(String browser) {

		switch(browser) {
		//		case "Chrome": System.setProperty("webdriver.chrome.driver", "./exefiles/chromedriver.exe");
		case "Chrome": driver = new ChromeDriver();
		break;

		case "FireFox": driver = new FirefoxDriver();
		break;

		case "IE": driver = new InternetExplorerDriver();
		break;

		default:driver = new ChromeDriver();
		}

	}

	// To get driver
	public WebDriver getDriver() {
		return driver;
	}

	public void setNullBrowserInstance() {
		instanceOfTestBrowserClass=null;
	}

}
