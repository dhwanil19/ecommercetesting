package scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import utilities.InitializeTestBrowser;

public class SearchPage {

	WebDriver driver;

	public SearchPage() {
		String testBrowser = System.getProperty("testBrowser");
		InitializeTestBrowser itb = InitializeTestBrowser.getinstanceOfTestBrowserClass(testBrowser);
		this.driver = itb.getDriver();
	}

	public void AddToCart(String mobileName, int ram, int price) {
		System.out.println("Adding phone - "+ mobileName + " with RAM - " + ram + " and price Rs. " + price);
	}

	public void SearchMobilePhone(String mobileName, int ram) throws InterruptedException {
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='twotabsearchtextbox']")).sendKeys(mobileName);
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id='nav-search']/form/div[2]/div/input")).click();

		System.out.println("Searching phone - "+ mobileName + " with RAM - " + ram);
	}
}
