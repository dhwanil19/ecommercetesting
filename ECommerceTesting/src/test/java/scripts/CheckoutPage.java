package scripts;

import org.openqa.selenium.WebDriver;

import utilities.InitializeTestBrowser;

public class CheckoutPage {

	WebDriver driver;
	InitializeTestBrowser itb;
	public CheckoutPage() {
		String testBrowser = System.getProperty("testBrowser");
		itb = InitializeTestBrowser.getinstanceOfTestBrowserClass(testBrowser);
		this.driver = itb.getDriver();
	}

	public void Checkout() {
		System.out.println("Proceeding to checkout");
	}

	public void CloseBrowser() {
		driver.quit();
		itb.setNullBrowserInstance();
	}
}
