package scripts;

import org.openqa.selenium.WebDriver;

import utilities.InitializeTestBrowser;

public class HomePage {
	WebDriver driver;

	public HomePage() {
		String testBrowser = System.getProperty("testBrowser");
		InitializeTestBrowser itb = InitializeTestBrowser.getinstanceOfTestBrowserClass(testBrowser);
		this.driver = itb.getDriver();
	}

	public void LoginToAmazon() {
		// System.setProperty("webdriver.chromedriver.driver",
		// "C:\\Users\\dhwan\\SeleniumDrivers\\chromedriver.exe");

		driver.get("https://www.amazon.in");
		System.out.println("Inside Login page");
	}
}
